// console.log("Hello Sir Tolits!")

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(responseJson => {console.log(responseJson.map(post => post.title))});

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(responseJson => console.log(`The status of ${responseJson.title} is ${responseJson.completed}.`));

fetch("https://jsonplaceholder.typicode.com/todos", 
			{
				method: "POST",
				headers: {
					"Content-Type" : "application/json"
				},
				body: JSON.stringify({
					title: "Created To Do List Item ",
					completed: false,
					userId: 1
				})
			}
		)
	.then(response => response.json())
	.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"},
	body: JSON.stringify({
			title : "Updated To Do List Item",
			description : "To update the my to do list with a different data structure use POST method.",
			status : "Pending",
			dateCompleted : "Pending",
			userId: 1
	})
})
	.then(response => response.json())
	.then(json => console.log(json))

	fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"},
	body: JSON.stringify({
		
		status : "Complete",
		dateCompleted : "07/09/21",
	})
})
	.then(response => response.json())
	.then(json => console.log(json))

	fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "DELETE"})
	.then(response => response.json())
	.then(json => console.log(json));
